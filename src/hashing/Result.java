
package hashing;

/**
 * @author Tom
 */
class Result {

    String[] hashTable;
    int hashTableSize;
    int itemCount;
    String hashMethodName;
    String collisionResolutionMethodName;
    long hashTime;
    long searchTime;
    int probeCount;

    public Result() {
        hashTableSize = 0;
        hashMethodName = "";
        collisionResolutionMethodName = "";
        hashTime = 9999;
        searchTime = 9999;
    }

    public float getLoadFactor() {
        return (float) itemCount / hashTableSize;
    }

    public void getResults() {
        itemCount = 0;
        /**
         * render a visual representation of the hash table distribution /**
         *
         */

        for (int i = 0; i < hashTableSize; i++) {
            if (i % 240 == 0) {
                System.out.println("");
            }
            if (hashTable[i].isEmpty()) {
                System.out.print("+");
            } else {
                System.out.print("W");
                itemCount++;
            }
        }
        System.out.println(" SUCCESS: Rendered the distubution map");
        System.out.println(" TABLE SIZE: " + hashTableSize);
        System.out.println(" Hash table creation time(ms)= " + (hashTime));
        System.out.println(" Hash table search time(ms)= " + (searchTime));
        System.out.println(" Max depth count: " + probeCount);
        System.out.println(" population count: " + itemCount);
        System.out.println(" LOAD FACTOR: " + getLoadFactor());
        System.out.println(" Hash Method : " + hashMethodName);
        System.out.println(" Collision Resolution Method : " + collisionResolutionMethodName);
        System.out.println("");
        System.out.println("##NEXT##");

    }

}
