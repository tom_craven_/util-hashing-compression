/*
 * U080282 Algorythms and datastructures
 * Coursework1 Semester One 2016/17 : Hashing
 */
package hashing;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * @author Tom Craven SId: 11070228
 */
public class Hashing {

    final static String DICTIONARYPATH = "src//hashing//dictionary.txt";
    final static String INPUTTEXTPATH = "src//hashing//test.txt";
    final static String HASHPRIMERSPATH = "src//hashing//primes.txt";

    static MyDictionary md;
    static Result bestResult;

    public Hashing() {
    }

    public static void main(String[] args) {
        bestResult = new Result();
        for (int tablesize = 13; tablesize < 14; tablesize++) { 
            
/**
12= 98317
13= 196613
14= 393241
15= 786433
16= 1572869
17=3145739
18=6291469
19=12582917
20=25165843
Any higher will likely exceed memory limits
 */
            for (int hashType = 1; hashType < 6; hashType++) { //  5 hash methods
                for (int collisionResolutionType = 1; collisionResolutionType < 4; collisionResolutionType++) { //3 collision resolution methods
                    md = new MyDictionary(tablesize, hashType, collisionResolutionType);
                    KeepQuickestResult(md.result);
                }
                bestResult.getResults();
            }
        }
    }

    private static void log(String number) {
        System.out.println(number);
    }

    static void KeepQuickestResult(Result newResult) {
        if (newResult.hashTime < bestResult.hashTime) {
            bestResult = newResult;
            System.out.println("NEW BEST RECORD!");
        }
    }

    static final class MyDictionary {

        Result result;
        ArrayList<Integer> primeNumbersList;

        long hashStartTime;
        long hashEndTime;
        long searchStartTime;
        long searchEndTime;

        /*
         * CLASS CONSTRUCTION
         * 
         * @Param HashTableSize : row number on integer taken from primes.txt
         * @Param hashMethod : case number from hash methods switch
         * @Param collisionResolutionMethod : case number from collision resolution switch
         *
         */
        MyDictionary(int HashTableSize, int hashMethod, int collisionResolutionMethod) {
            result = new Result();
            ReadPrimers();
            ReadDictionaryMakeHashTable(HashTableSize, hashMethod, collisionResolutionMethod);
            ReadTextDoSpellCheck(hashMethod, collisionResolutionMethod);

        }

        /**
         * Read in the list of table sizes, in this case prime numbers.
         */
        void ReadPrimers() {
            primeNumbersList = new ArrayList<>();
            try (Stream<String> stream = Files.lines(Paths.get(HASHPRIMERSPATH))) {
                stream.forEach((t)
                        -> {
                    primeNumbersList.add(Integer.parseInt(t));
                });
            } catch (IOException e) {
                log("The text file containing the primers has not loaded, check the file src//util//hashing//primes.txt exists");
            }
        }

        /**
         * read the text file containing the dictionary and make the hash table
         */
        void ReadDictionaryMakeHashTable(int HashTableSize, int hashMethod, int collisionResolutionMethod) {
            /**
             * INIT TABLE
             */
            result.hashTableSize = primeNumbersList.get(HashTableSize);
            result.hashTable = new String[result.hashTableSize];
            for (int i = 0; i < result.hashTableSize; i++) {
                result.hashTable[i] = "";
            }
            /**
             * GET LINES
             */

            try (Stream<String> stream = Files.lines(Paths.get(DICTIONARYPATH))) {
                hashStartTime = System.currentTimeMillis();
                stream.forEach((t)
                        -> {
                    int hashIndex;
                    result.probeCount = 1;
                    /**
                     * SELECT HASH METHOD
                     */
                    hashIndex = hashMethodSwitch(hashMethod, collisionResolutionMethod, t);
                    while (!"".equals(result.hashTable[hashIndex])) {
                        /**
                         * SELECT COLLISION RESOLUTION METHOD
                         */
                        hashIndex = collisionResolutionSwitch(result.probeCount, hashIndex, collisionResolutionMethod);

                        result.probeCount++;
                    }
//                log("probed" + probeCount);
//                log("inserted" + hashIndex);
                    result.hashTable[hashIndex] = t;
                });
                hashEndTime = System.currentTimeMillis();
                result.hashTime = hashEndTime - hashStartTime;
            } catch (IOException e) {
                log("The text file containing the dictionary has not loaded, check the file src//util//hashing//dictionary.txt exists");
            }
        }

        /**
         * Read in the text document to be checked for spelling mistakes
         */
        void ReadTextDoSpellCheck(int hashMethod, int collisionResolutionMethod) {
            try (Stream<String> stream = Files.lines(Paths.get(INPUTTEXTPATH))) {
                System.out.println("##SPELL CHECKER##");
                searchStartTime = System.currentTimeMillis();
                stream.forEach(
                        (t) -> {
                            if (DoSpellCheck(hashMethod, collisionResolutionMethod, t) == true) {
                                System.out.println("found word " + t);
                            } else {
                                System.out.println(t + " cannot be found");
                            }
//                          log("Success : You can spell! ms: " + (endTime - startTime));
                        });
                searchEndTime = System.currentTimeMillis();
                result.searchTime = searchEndTime - searchStartTime;
            } catch (IOException e) {
                log("The text file containing text to be spell checked has not loaded, check the file src//util//hashing//test.txt exists");
            }
        }

        /*
     * FUNCTIONS 
         */
        /**
         * check the spelling by searching for the word in the hashTable
         */
        boolean DoSpellCheck(int hashMethod, int collisionResolutionMethod, String keyWord) {
            int hashKey = hashMethodSwitch(hashMethod, collisionResolutionMethod, keyWord);
            int probeCount = 1;
            while (!"".equals(result.hashTable[hashKey])) {
                if (keyWord.equals(result.hashTable[hashKey])) {
                    return true;
                } else {
                    hashKey = collisionResolutionSwitch(probeCount, hashKey, collisionResolutionMethod);
                    probeCount++;
                }
            }
            return false;
        }

        int hashMethodSwitch(int hashMethod, int collisionResolutionMethod, String wordKey) {
            int i;
            switch (hashMethod) {
                case 1:
                    i = truncationHash(result.hashTableSize, wordKey);
                    result.hashMethodName = "truncation";
                    break;
                case 2:
                    i = divisionHash(result.hashTableSize, wordKey);
                    result.hashMethodName = "division";

                    break;
                case 3:
                    i = fibonacciHash(result.hashTableSize, wordKey);
                    result.hashMethodName = "fibonacci";
                    break;
                case 4:
                    i = divisionHashAndProbe(result.hashTableSize, wordKey);
                    result.hashMethodName = "division and probe";
                    break;
                case 5:
                    i = fibonacciHashAndProbe(result.hashTableSize, wordKey);
                    result.hashMethodName = "fibonacci and probe";
                    break;
                default:
                    i = 0;
            }
            return i;
        }

        int collisionResolutionSwitch(int probeCount, int hashKey, int collisionResolutionMethod) {
            int collisionResolutionSwitch;
            switch (collisionResolutionMethod) {
                case 1:
                    collisionResolutionSwitch = doubleHash(probeCount, result.hashTableSize, hashKey);
                    result.collisionResolutionMethodName = "doubleHash";
                    break;
                case 2:
                    collisionResolutionSwitch = linearProbe(probeCount, result.hashTableSize, hashKey);
                    result.collisionResolutionMethodName = "linearProbe";
                    break;
                case 3:
                    collisionResolutionSwitch = quadraticProbe(probeCount, result.hashTableSize, hashKey);
                    result.collisionResolutionMethodName = "quadraticProbe";
                    break;
                default:
                    collisionResolutionSwitch = 0;
            }
            return collisionResolutionSwitch;
        }

        /**
         * HASH FUNCTIONS
         */
        int divisionHash(int hashTableSize, String wordKey) {
            int divisionHash = 0;
            for (char ch : wordKey.toCharArray()) {
                divisionHash = divisionHash + Character.getNumericValue(ch);
            }
            divisionHash = (divisionHash % hashTableSize);
            return divisionHash;
        }

        int divisionHashAndProbe(int hashTableSize, String wordKey) {
            int divisionHashAndProbe = 0;
            int exponent = 1;
            for (char ch : wordKey.toCharArray()) {

                divisionHashAndProbe = divisionHashAndProbe + ((Character.getNumericValue(ch) * (31 ^ exponent)));
                exponent++;
            }
            divisionHashAndProbe = (divisionHashAndProbe % hashTableSize);
            return divisionHashAndProbe;
        }

        int truncationHash(int hashTableSize, String wordKey) {

            int truncationHash = Character.getNumericValue(wordKey.charAt(wordKey.length() - 1))
                    + Character.getNumericValue(wordKey.charAt(0));
            truncationHash = (truncationHash % hashTableSize);
            return truncationHash;
        }

        int fibonacciHash(int hashTableSize, String wordKey) {

            int fibonacciHash = 0;
            if (wordKey.length() == 2) {
                fibonacciHash = Character.getNumericValue(wordKey.charAt(0));
            } else {
                for (int i = 3; i <= wordKey.length(); i++) {

                    fibonacciHash = fibonacciHash + (Character.getNumericValue(wordKey.charAt(i - 1)) + Character.getNumericValue(wordKey.charAt(i - 2)));
                }
            }
            fibonacciHash = (fibonacciHash % hashTableSize);
            return fibonacciHash;
        }

        int fibonacciHashAndProbe(int hashTableSize, String wordKey) {
            int exponent = 1;
            int fibonacciHashAndProbe = 0;
            if (wordKey.length() == 2) {//  || wordKey.length() == 1  *Excersise data contains no one letter words*
                fibonacciHashAndProbe = Character.getNumericValue(wordKey.charAt(0));
            } else {
                for (int i = 3; i <= wordKey.length(); i++) {
                    // dereference hash function from character value in focus by using the context of the word<-- reduced clash probability! 
                    fibonacciHashAndProbe = fibonacciHashAndProbe + ((Character.getNumericValue(wordKey.charAt(i - 1))
                            * (31 ^ exponent)) + (Character.getNumericValue(wordKey.charAt(i - 2)) * (31 ^ exponent)));
                    exponent++;
                }
            }
            fibonacciHashAndProbe = (fibonacciHashAndProbe % hashTableSize);
            return fibonacciHashAndProbe;
        }

        /**
         * COLLISION RESOLUTION
         */
        int doubleHash(int probeCount, int hashTableSize, int key) {
            int doubleHash = key + (probeCount * 31);
            doubleHash = (doubleHash % hashTableSize);
            return doubleHash;
        }

        int quadraticProbe(int probeCount, int hashTableSize, int key) {
            int quadraticProbe = key + (probeCount ^ 2);
            quadraticProbe = (quadraticProbe % hashTableSize);
            return quadraticProbe;
        }

        int linearProbe(int probeCount, int hashTableSize, int key) {
            int linearProbe = key + probeCount;
            linearProbe = (linearProbe % hashTableSize);
            return linearProbe;
        }
    }
}
