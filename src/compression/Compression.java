/*
 * U080282 Algorithms and data structures
 * Coursework2 Semester One 2016/17 : Compression
 */
package compression;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.stream.Stream;

/**
 * @author Tom Craven SId: 11070228
 */
public class Compression extends CompressionService {

    public static void main(String[] args) {
        Compression result = new Compression();
        result.streamFileLines();
    }

    final static String PATHTOTEXTFILE = "src//compression//test.txt";
    private static final long MAXSIZE = 14l;
    private int losses;
    private int total;

    /*
        STEP#A Root method streams strings (t) from designated text file,
     */
    void streamFileLines() {

        try (Stream<String> stream = Files.lines(Paths.get(PATHTOTEXTFILE))) {
            total = 0;
            losses = 0;
            /*
            STEP Filter a specified max string length 
             */
            stream.filter(t -> t.length() <= MAXSIZE).forEach((t)
                    -> {
                reset();
                log("String to encode: ".concat(t));
                /*
                STEP Make a frequency table 
                 */
                doFrequencyTable(t);
                /*
                STEP Create an object for each character
                 */
                doSetupObjects();
                /*
                STEP Compress encode string as java double
                 */
                encodedValue = doCompress(t);
                log("Encoded value is: " + encodedValue);
                /*
                STEP decode string String. cheating method was a development aid
                 */
                decodedString
                        //                        = deCompressCheating();
                        = deCompressNotCheating();
                log("Decoded value is " + decodedString + " @Length "
                        + decodedString.length());
                /*
                STEP Detect losses
                 */
                if (!t.equals(decodedString)) {
                    log("###LOSSY###@Length " + t.length());
                    losses += 1;
                }
                log("");
                total += 1;
            });
            log("Out of " + total + " words " + losses + " losses detected");
        } catch (IOException e) {
            log("The text file containing the text failed to load");
        }
    }

    private void reset() {
        encodedValue = 0d;
        frequencyTable.clear();
        high = 1d;
        low = 0d;
        range = 0d;
        index = 0;
        objSymbols.clear();
        decodedString = "";
    }

}
