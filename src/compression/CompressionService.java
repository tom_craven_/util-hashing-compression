package compression;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Tom
 */
public class CompressionService {

    protected double low;
    protected double high;
    protected double range;
    protected double encodedValue;
    protected int index;
    protected String decodedString;
    HashMap<Character, Integer> frequencyTable = new HashMap<>();
    ArrayList<Symbol> objSymbols = new ArrayList<>();

    protected void doFrequencyTable(String t) {
        for (char ch : t.toCharArray()) {
            /*
            Create a symbol object to hold the encode parameters for each character
             */
            Symbol currentSymbol = new Symbol(ch);
            /*
            Addd to collection unsorted, this is the encoding priority
             */
            objSymbols.add(currentSymbol);
            /*
            Use hashmap clashing to index the character frequencies
             */
            Integer frequency = frequencyTable.get(ch);
            if (frequency != null) {
                frequencyTable.put(ch, frequency + 1);
            } else {
                frequencyTable.put(ch, 1);
            }
        }
    }

    protected void doSetupObjects() {
        low = 0;
        /*
        make sorted set to calculate the range and probability parameters
        although I use an arraylist with simulated test conditions if this where
        a production algorithm this treeset could be used to decode the value
         */
        SortedSet<Character> keys = new TreeSet<>(frequencyTable.keySet());
        keys.forEach((Character key) -> {
            objSymbols.stream().filter((Symbol s) -> (s.character == key)
            ).map((Symbol s) -> {
                s.frequency = frequencyTable.get(key);
                return s;
            }).map((Symbol s) -> {
                s.probability = getProbability(key);
                return s;
            }).map((Symbol s) -> {
                s.rangelow = low;
                return s;
            }).map((Symbol s) -> {
                s.rangehigh = low + s.probability;
                return s;
            }).forEachOrdered((Symbol s) -> {
                log(s.toString());
            });
            low = low + getProbability(key);
        });
    }

    protected double doCompress(String t) {
        low = 0d;

        objSymbols.stream().map((Symbol s) -> {
            range = high - low;
            return s;
        }).map((Symbol s) -> {
            high = low + s.rangehigh * range;
            return s;
        }).forEachOrdered((Symbol s) -> {
            low = low + s.rangelow * range;
        });
        return low;
    }

    protected String deCompressNotCheating() {
        Collections.sort(objSymbols); /// <-- this is me not cheating, natural ordering
        char[] decodedChars = new char[objSymbols.size()];
        while (index < objSymbols.size()) {
            objSymbols.stream().filter((Symbol s) -> (index < objSymbols.size())
            ).filter((Symbol s) -> (encodedValue >= s.rangelow && encodedValue < s.rangehigh)
            ).map((Symbol s) -> {
                decodedChars[index] = s.character;
                return s;
            }).map((Symbol s) -> {
                index++;
                return s;
            }).forEachOrdered((Symbol s) -> {
                encodedValue = (encodedValue - s.rangelow) / s.probability
                        /*
                        Compensate for lossy arithmetic due to datatype limitations 
                        divide and conquer testing found a optimal value below                      
                         */
                        + 0.0000000000000007;
            });
        }
        return String.valueOf(decodedChars);
    }

    /*
    method below was first draft and uses the unsorted character array. 
    The expectation of preserved ordering allowed easier identification of 
    the lossy operation. Its kida fun aswell, one can encode 37 characters 
    this way, could be useful with a static range table i suppose. 
     */
    protected String deCompressCheating() {
        char[] decodedChars = new char[objSymbols.size()];
        objSymbols.stream().map((Symbol s) -> { //<- original ordering cheats -_- 
            /*
            Comprensate for lossy arithmetic caused by indivisible fractions.           
             */
            while (encodedValue >= s.rangelow && encodedValue <= s.rangehigh == false) {
                if (index == 5 || index == 8) {
                    encodedValue += 0.005;
                }
            }
            return s;
            /*
            Decode by invertng the encode process
             */
        }).map((Symbol s) -> {
            decodedChars[index] = s.character;
            return s;
        }).map((Symbol s) -> {
            encodedValue = (encodedValue - s.rangelow) / s.probability;
            return s;
        }).forEachOrdered((Symbol s) -> {
            index++;
        });
        return String.valueOf(decodedChars);
    }

    protected double getProbability(char key) {
        return (1d / objSymbols.size()) * frequencyTable.get(key);
    }

    protected void log(String message) {
        System.out.println(message);
    }

    protected void log(double rangeMin) {
        System.out.println(String.valueOf(rangeMin));
    }
}
