
package compression;

/**
 * @author Tom
 */
class Symbol  implements Comparable<Symbol> {
    int frequency;    
    char character;
    double probability;
    double rangehigh;
    double rangelow;  

    public Symbol(char character) {
        this.character = character;
    }

    public Integer getCharacter() {
        return Integer.valueOf(character);
    } 

    @Override
    public String toString() {
        return "Symbol{" + "frequency=" + frequency + ", character=" 
                + character + ", probability=" + probability + ", rangehigh=" 
                + rangehigh + ", rangelow=" + rangelow + '}';
    }

    @Override
    public int compareTo(Symbol o) {
       return getCharacter().compareTo(o.getCharacter());
    }
    }
